import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DivisorTest {

	@Test
	void dividirUnNumeroPorSiMismo() {
		assertEquals(1, Divisor.dividorDe(4, 4));
	}

	@Test
	void dividirDosNumeros() {
		assertEquals(5, Divisor.dividorDe(20, 4));
	}
	
	@Test
	void dividirPorCero() {
		Assertions.assertThrows(ArithmeticException.class, () -> Divisor.dividorDe(4, 0));
	}
}
